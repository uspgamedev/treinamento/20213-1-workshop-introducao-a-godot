# Workshop de Introdução à Godot

## Sumário

- [Instalação da Godot](#instalação-da-godot)

- [Explicações sobre a interface](#explicações-sobre-a-interface)

- [Explicações sobre o esquema de nós](#explicações-sobre-o-esquema-de-nós)

- [Mini-projetos](#mini-projetos)

    - [Mostrando alguma coisa na tela](#mostrando-alguma-coisa-na-tela)

    - [Movimentando a coisa na tela de forma não interativa](#movimentando-a-coisa-na-tela-de-forma-não-interativa)

    - [Movimentando a coisa na tela de forma interativa](#movimentando-a-coisa-na-tela-de-forma-interativa)

    - [Pegando input de controle](#pegando-input-de-controle)

    - [Adicionando colisões](#adicionando-colisões)

- [Projeto final: Jogo de Plataforma](#projeto-final-jogo-de-plataforma)

    - [Modificando o movimento](#modificando-o-movimento)

    - [Mudando o mundo](#mudando-o-mundo)

    - [Adicionando gravidade](#adicionando-gravidade)

    - [Adicionando pulo](#adicionando-pulo)
    
    - [Adicionando câmera](#adicionando-câmera)

    - [Adicionando tiros](#adicionando-tiros)

    - [Adicionando inimigo que toma dano](#adicionando-inimigo-que-toma-dano)

- [Conteúdo extra para estudo](#conteúdo-extra-para-estudo)

## Instalação da Godot

- Acesse a [página de downloads da Godot 3](https://godotengine.org/download/3.x/)

- Faça o download clicando no botão azul `Godot Engine`

- Salve em algum lugar que você vai se lembrar

- Extraia os arquivos do `.zip` que foi baixado

- Abra o executável

## Explicações sobre a interface

- A explicação sobre a interface está presente na gravação do workshop

## Explicações sobre o esquema de nós

- A explicação sobre o esquema de nós está presente na gravação do workshop

## Mini-projetos

### Mostrando alguma coisa na tela

- Crie uma cena nova chamada `Mundo` com root `Node2D`

- Salve a cena com nome `mundo.tscn`

- Adicione um nó `Sprite` como filho de `Mundo`

- Arraste o `icon.png` para a aba de `Texture` do `Sprite`

- Execute o programa

### Movimentando a coisa na tela de forma não interativa

- Renomeie o nó `Sprite2D` para `Jogador`

- Adicione um script no nó `Jogador` chamado `jogador.gd`

```gdscript
extends Sprite


func _ready() -> void:
    pass


func _process(delta: float) -> void:
    self.position.x += 10.0;
```

- Rode o jogo e veja ele se movendo!

- Devemos usar o valor de `delta` da função para que o movimento passe a ser independente do `fps` do jogo

```gdscript
extends Sprite


func _ready() -> void:
    pass


func _process(delta: float) -> void:
    self.position.x += 200.0 * delta;
```

- Se você rodar o jogo agora, provavelmente não vai notar muita diferença, mas agora o movimento está acontecendo de forma independente do `fps`

- Podemos extrair também o valor `200` em uma variável exportada no editor, para facilidade de modificação

```gdscript
extends Sprite

export(float) var velocidade := 200.0;


func _ready() -> void:
    pass


func _process(delta: float) -> void:
    self.position.x += velocidade * delta;
```

### Movimentando a coisa na tela de forma interativa

- Navegue para `Project > Project Settings > Input Map`

- Adicione ações
  
  - `jogador_esquerda`
  
  - `jogador_direita`
  
  - `jogador_cima`
  
  - `jogador_baixo`

- Adicione teclas para essas ações

- Atualize o script `jogador.gd` para utilizar os novos inputs

```gdscript
extends Sprite

export(float) var velocidade := 200.0;


func _physics_process(delta: float) -> void:
    var direcao := Vector2.ZERO;
    if Input.is_action_pressed("jogador_direita"):
        direcao.x += 1;
    if Input.is_action_pressed("jogador_esquerda"):
        direcao.x += -1;
    if Input.is_action_pressed("jogador_baixo"):
        direcao.y += 1
    if Input.is_action_pressed("jogador_cima"):
        direcao.y += -1;

    self.position += direcao.normalized() * velocidade * delta;
```

- Podemos extrair o código de pegar a direção do movimento para sua própria função

```gdscript
extends Sprite

export(float) var velocidade := 200.0;

var direcao := Vector2.ZERO;


func pega_input() -> void:
    direcao = Vector2.ZERO;
    if Input.is_action_pressed("jogador_direita"):
        direcao.x += 1;
    if Input.is_action_pressed("jogador_esquerda"):
        direcao.x += -1;
    if Input.is_action_pressed("jogador_baixo"):
        direcao.y += 1;
    if Input.is_action_pressed("jogador_cima"):
        direcao.y += -1;


func _physics_process(delta: float) -> void:
    pega_input();
    self.position += direcao.normalized() * velocidade * delta;
```

### Pegando input de controle

- Adicione os analógicos do controle para as ações de input definidas no tópico anterior

- Atualize o deadzone das ações

- Atualize o código de input do `jogador.gd` para utilizar as funcionalidade do analógico

```gdscript
extends Sprite

export(float) var velocidade := 200.0;

var direcao := Vector2.ZERO;


func pega_input() -> void:
    direcao = Vector2.ZERO;
    direcao.x = Input.get_action_strength("jogador_direita") - Input.get_action_strength("jogador_esquerda");
    direcao.y = Input.get_action_strength("jogador_baixo") - Input.get_action_strength("jogador_cima");


func _physics_process(delta: float) -> void:
    pega_input();
    if direcao.length_squared() > 1: # Isso é para fazer o movimento ter sensibilidade com o analógico
        direcao = direcao.normalized();
    self.position += direcao * velocidade * delta;
```

### Adicionando colisões

- Vamos criar uma nova cena chamada `Jogador` baseado em um `KinematicBody2D`

- Adicione um `Sprite` como filho de `Jogador` e coloque a imagem `icon.png` no campo `Texture`

- Adicione um `CollisionShape2D` como filho de `Jogador`

- Adicione uma `RectangleShape2D` ao campo `Shape` de `CollisionShape2D` e redimensione ela para ter o mesmo tamanho da `Sprite`

- Arraste o script `jogador.gd` do explorador de arquivos até o nó `Jogador`

- Edite o script para funcionar com um `KinematicBody2D`

```gdscript
extends KinematicBody2D

export(float) var velocidade := 200.0;

var direcao := Vector2.ZERO;


func pega_input() -> void:
    direcao = Vector2.ZERO;
    direcao.x = Input.get_action_strength("jogador_direita") - Input.get_action_strength("jogador_esquerda");
    direcao.y = Input.get_action_strength("jogador_baixo") - Input.get_action_strength("jogador_cima");


func _physics_process(delta: float) -> void:
    pega_input();
    if direcao.length_squared() > 1: # Isso é para fazer o movimento ter sensibilidade com o analógico
        direcao = direcao.normalized();
    self.position += direcao * velocidade * delta;
```

- Delete o nó `Jogador` da cena `World`

- Adicione 3 `StaticBody2D` à cena `World` e adicione `CollisionShape2D`s a cada um dos `StaticBody2D`

- Arraste o arquivo `jogador.tscn` do explorador de arquivos para a árvore de nós da cena `World` para instanciar um jogador

- Perceba que, se rodar o jogo agora, as colisões não vão ocorrer ainda
  
  - Para isso, precisamos fazer mais algumas alterações no código do jogador

```gdscript
extends KinematicBody2D

export(float) var velocidade := 200.0;

var direcao := Vector2.ZERO;


func pega_input() -> void:
    direcao = Vector2.ZERO;
    direcao.x = Input.get_action_strength("jogador_direita") - Input.get_action_strength("jogador_esquerda");
    direcao.y = Input.get_action_strength("jogador_baixo") - Input.get_action_strength("jogador_cima");


func _physics_process(_delta: float) -> void:
    pega_input();
    if direcao.length_squared() > 1: # Isso é para fazer o movimento ter sensibilidade com o analógico
        direcao = direcao.normalized();
    self.move_and_slide(direcao * velocidade);
```

## Projeto final: Jogo de Plataforma

### Modificando o movimento

- Na movimentação de plataforma, o jogador se movimenta com as setas apenas da esquerda para a direita

```gdscript
extends KinematicBody2D

export(float) var velocidade := 200.0;

var direcao := Vector2.ZERO;


func pega_input() -> void:
    direcao = Vector2.ZERO;
    direcao.x = Input.get_action_strength("jogador_direita") - Input.get_action_strength("jogador_esquerda");


func _physics_process(_delta: float) -> void:
    pega_input();
    if direcao.length_squared() > 1:
        direcao = direcao.normalized();
    self.move_and_slide(direcao * velocidade);
```

### Mudando o mundo

- Precisamos atualizar também a cena `World` para representar melhor um jogo de plataforma
  
  - Modifique os `StaticBody2D`s da cena para melhor refletir um jogo de plataforma

### Adicionando gravidade

- Para um jogo de plataforma, também é necessário que haja gravidade
  
  - Isso requer algumas modificações no código do jogador

```gdscript
extends KinematicBody2D

export(float) var gravidade := 20.0;
export(float) var velocidade := 200.0;

var direcao := Vector2.ZERO;
var movimento := Vector2.ZERO;


func pega_input() -> void:
    direcao = Vector2.ZERO;
    direcao.x = Input.get_action_strength("jogador_direita") - Input.get_action_strength("jogador_esquerda");


func _physics_process(_delta: float) -> void:
    pega_input();
    if direcao.length_squared() > 1:
        direcao = direcao.normalized();
    movimento.x = direcao.x * velocidade;
    movimento.y += gravidade * delta;
    movimento = self.move_and_slide(movimento);
```

### Adicionando pulo

- Navegue para `Project > Project Settings > Input Map` e adicione uma nova ação `jogador_pulo`

- Mapeie alguma tecla/botão do controle para essa ação

- Modifique o código do jogador para utilizar o pulo

```gdscript
extends KinematicBody2D

export(float) var gravidade := 20.0;
export(float) var velocidade := 200.0;
export(float) var velocidade_pulo := 700.0;

var direcao := Vector2.ZERO;
var movimento := Vector2.ZERO;
var acabou_de_pular: bool = false;


func pega_input() -> void:
    direcao = Vector2.ZERO;
    direcao.x = Input.get_action_strength("jogador_direita") - Input.get_action_strength("jogador_esquerda");

    acabou_de_pular = Input.is_action_just_pressed("jogador_pulo") and self.is_on_floor();


func _physics_process(_delta: float) -> void:
    pega_input();
    if direcao.length_squared() > 1:
        direcao = direcao.normalized();
    movimento.x = direcao.x * velocidade;
    movimento.y += gravidade;

    if acabou_de_pular:
        movimento.y = -velocidade_pulo;

    movimento = self.move_and_slide(movimento, Vector2.UP);
```

### Adicionando câmera

- Adicione um nó `Câmera2D` à cena `Jogador` como filho do nó `Jogador`

- Marque a opção `Current` da `Câmera2D`

- Na aba `Smoothing` das propriedades da `Câmera2D`, marque a opção `Enabled`

- Marque as opções `Drag Margin H Enabled` e `Drag Margin V Enabled`

- Na aba `Limit`:
  
  - Coloque o valor de `-200` na propriedade `Left`
  
  - Coloque o valor de `-100` na propriedade `Top`
  
  - Coloque o valor de `1224` na propriedade `Right`
  
  - Coloque o valor de `700` na propriedade `Bottom`
  
  - Marque a opção `Smoothed`
  
  - Note que esses valores devem ser modificados dependendo das condições do seu nível
    
    - Você pode testar esses valores arrastando o nó `Jogador` na cena `World` no editor da Godot

### Adicionando tiros

- Crie uma cena chamada `Bala` que herde de `Area2D`

- Adicione um `Sprite` como filho de `Bala` e arraste o arquivo `icon.png` para o campo `Texture`

- Adicione uma `CollisionShape2D` como filho de `Bala` e adicione uma `RectangleShape2D` no campo `Shape`
  
  - Redimensione a colisão para ficar do mesmo tamanho do `Sprite`

- Adicione um script `bala.gd` no nó `Bala`

```gdscript
class_name Bala
extends Area2D

export(float) var velocidade := 1000.0;

var direcao := 1;

func _physics_process(delta: float) -> void:
    self.position.x += direcao * velocidade * delta;

    if abs(self.position.x > 1000): self.queue_free();
```

- Agora, navegue para `Project > Project Settings > Input Map` e adicione uma ação de input chamada `jogador_tiro`
  
  - Adicione uma tecla/botão do controle para essa ação

- Modifique o código do jogador para utilizar a ação e atirar

```gdscript
extends KinematicBody2D

export(PackedScene) var bala_cena;
export(float) var gravidade := 20.0;
export(float) var velocidade := 200.0;
export(float) var velocidade_pulo := 700.0;

var direcao := Vector2.ZERO;
var olhando_para := 1;
var movimento := Vector2.ZERO;
var acabou_de_pular: bool = false;
var acabou_de_atirar: bool = false;


func pega_input() -> void:
    direcao = Vector2.ZERO;
    direcao.x = Input.get_action_strength("jogador_direita") - Input.get_action_strength("jogador_esquerda");

    acabou_de_pular = Input.is_action_just_pressed("jogador_pulo") and self.is_on_floor();
    acabou_de_atirar = Input.is_action_just_pressed("jogador_tiro");


func _physics_process(_delta: float) -> void:
    pega_input();
    if direcao.length_squared() > 1:
        direcao = direcao.normalized();

    if abs(direcao.x) > 0:
        olhando_para = sign(direcao.x) as int;

    movimento.x = direcao.x * velocidade;
    movimento.y += gravidade;

    if acabou_de_pular:
        movimento.y = -velocidade_pulo;

    if acabou_de_atirar:
        var nova_bala := bala_cena.instance() as Bala;
        self.get_parent().add_child(nova_bala);
        nova_bala.direcao = olhando_para;
        nova_bala.global_position = self.global_position;

    movimento = self.move_and_slide(movimento, Vector2.UP);
```

### Adicionando inimigo que toma dano

- Crie uma nova cena chamada `Inimigo` com base em uma `Area2D`

- Adicione um `Sprite` como filho do nó `Inimigo` e adicione a imagem `icon.png` ao campo `Texture`
  
  - No campo `Visibility` modifique a cor da opção `Modulate` para uma cor vermelha

- Adicione uma `CollisionShape2D` como filha do nó `Inimigo` e adicione uma `RectangleShape2D` no campo `Shape`
  
  - Redimensione o retângulo para ficar do mesmo tamanho do `Sprite`

- Adicione alguns inimigos na cena `World`

- Adicione um script `inimigo.gd` ao nó `Inimigo`

- Selecione o nó `Inimigo` na árvore de nós, depois, na aba `Node` do inspetor, conecte o sinal `area_entered` no `Inimigo`

```gdscript
extends Area2D


func _on_Inimigo_area_entered(area: Area2D) -> void:
    pass
```

- Agora, modifique o script `inimigo.gd` para adicionar lógica de vida e morte

```gdscript
extends Area2D

var vida := 5;


func _on_Inimigo_area_entered(area: Area2D) -> void:
    if area is Bala:
        vida -= 1;
        area.queue_free();

        if vida == 0: morre();


func morre() -> void:
    print("inimigo morreu!!");
    self.queue_free();
```

## Conteúdo extra para estudo

- [Jogo do GDQuest sobre godot](https://gdquest.itch.io/learn-godot-gdscript): Uma aula interativa feita pelo pessoal da [GDQuest](https://www.youtube.com/@Gdquest/featured) sobre os básicos de Godot e GDScript. Essencial para qualquer iniciante!

- [Documentação oficial da Godot](https://docs.godotengine.org/en/3.5/): Usem e abusem disso. A documentação da Godot é um dos melhores pontos dela e ta aí pra ser usado.