extends Area2D

signal morreu();

var vida: int = 5;

func _on_Inimigo_area_entered(area: Area2D) -> void:
	if area is Bala:
		area.queue_free();
		vida -= 1;
		if vida == 0:
			self.queue_free();
			self.emit_signal("morreu");


func _on_Inimigo_body_entered(body):
	if body is Jogador:
		print("O jogador está em cima de mim");
