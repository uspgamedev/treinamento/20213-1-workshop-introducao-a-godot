extends Node2D


func _ready():
	$Jogador.connect("atirou", self, "_on_jogador_atirou");
	$Jogador.connect("pulou", self, "on_jogador_pulou");


func _on_jogador_atirou(bala: Bala) -> void:
	print("Jogador atirou a bala ", bala);

func on_jogador_pulou() -> void:
	print("Jogador pulou!!");
