class_name Bala
extends Area2D

export var velocidade := 1000.0;

var direcao: int = 1;


func _physics_process(delta):
	self.position.x += direcao * velocidade * delta;
