class_name Jogador
extends KinematicBody2D

signal atirou(bala);
signal pulou();

export var cena_bala: PackedScene;
export var gravidade: float = 900.0;
export var velocidade: float = 200.0;

var direcao: Vector2;
var movimento: Vector2;
var olhando_para: int = -1;
var acabou_de_pular: bool = false;
var acabou_de_atirar: bool = false;


func pega_input() -> void:
	direcao.x = Input.get_action_strength("jogador_direita")\
				- Input.get_action_strength("jogador_esquerda");
	
	acabou_de_pular = Input.is_action_just_pressed("jogador_pulo") and self.is_on_floor();
	acabou_de_atirar = Input.is_action_just_pressed("jogador_tiro");


func _physics_process(delta: float) -> void:
	pega_input();
	if direcao.length() > 1:
		direcao = direcao.normalized();
	
	if abs(direcao.x) > 0:
		olhando_para = sign(direcao.x) as int;
	
	movimento.x = direcao.x * velocidade;
	movimento.y += gravidade * delta;
	
	if acabou_de_pular:
		movimento.y = -700;
		self.emit_signal("pulou");
	
	if acabou_de_atirar:
		var nova_bala := cena_bala.instance() as Bala;
		self.get_parent().add_child(nova_bala);
		nova_bala.global_position = self.global_position;
		nova_bala.direcao = olhando_para;
		self.emit_signal("atirou", nova_bala);
	
	movimento = self.move_and_slide(movimento, Vector2.UP);
